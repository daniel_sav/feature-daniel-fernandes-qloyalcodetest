package Lightbulb_API_Test;

import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import static org.hamcrest.CoreMatchers.equalTo;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.json.*;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.apache.log4j.Logger;
import org.junit.Assert;

public class APITest {
	 final static Logger LOGGER = Logger.getLogger(APITest.class);

	  @BeforeMethod
	  public static void setup() {
	   
	        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	        RestAssured.baseURI = "https://qa-challenges-lightbulb.atlassian.io";
	        RestAssured.basePath = "/api/allmethods/";
	    }


	// Turning switch on high power 
	@Test
	public void testswitchOnhighpower() throws InterruptedException {
	       JsonObject jsonbody = new JsonObject();
	        jsonbody.addProperty("power", "50");

	        LOGGER.info("Turning the light on with high power");

	        Response response = RestAssured.given().
	                header("Content-Type", "application/json\r\n").
	                header("userId", "253847534875ghj").
	                body(jsonbody.toString()).
	                when().
	                post("on").
	                then().
	                assertThat().
	                statusCode(200).
	                and().
	                extract().response();
	        String sResponse = response.jsonPath().getString("Result");
	       
	        Assert.assertThat("Switch on light with high power", sResponse, equalTo("Switch & custom power set successfully"));
	}


	
	// Turning switch on low power 
		@Test
		public void testswitchonlowpower() throws InterruptedException {
		       JsonObject jsonbody = new JsonObject();
		        jsonbody.addProperty("power", "20");

		        LOGGER.info("Turning the light on with low power");

		        Response response = RestAssured.given().
		                header("Content-Type", "application/json\r\n").
		                header("userId", "253847534875ghj").
		                body(jsonbody.toString()).
		                when().
		                post("on").
		                then().
		                assertThat().
		                statusCode(200).
		                and().
		                extract().response();
		        String sResponse = response.jsonPath().getString("Result");
		       
		        Assert.assertThat("Switch on light with low power", sResponse, equalTo("Switch & custom power set successfully"));
		}

		
		
		// Turning switch on default power 
				@Test
				public void testswitchondefaultpower() throws InterruptedException {
				      

				        LOGGER.info("Turning the light on with default power");

				        Response response = RestAssured.given().
				                header("Content-Type", "application/json\r\n").
				                header("userId", "8000904543jkkoj;").				              
				                when().
				                post("on").
				                then().
				                assertThat().
				                statusCode(200).
				                and().
				                extract().response();
				        String sResponse = response.jsonPath().getString("Result");
				       
				        Assert.assertThat("Switch on light with low power", sResponse, equalTo("Switch set successfully"));
				}
	// Turning switch  off
	
	@Test
	public void testswitchofflight() {
		   LOGGER.info("Testing switch off light");
	        Response response = RestAssured.given().
	                header("userId", "245892485934859etuy").
	                header("Content-Type", "application/json").
	                when().
	                post("off").
	                then().
	                assertThat().
	                statusCode(200).
	                and().
	                extract().response();
	        String resJsonAsString = response.jsonPath().getString("Result");
	     
	        Assert.assertThat("Switch off light", resJsonAsString, equalTo("Switch set successfully"));
	}


	  @AfterMethod
	  public void afterMethod() {
	  }

}
