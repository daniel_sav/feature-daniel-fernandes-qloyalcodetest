@Lightbulbtest
Feature: Lightbulbtest
 

  @SwitchOn
  Scenario Outline: Switching the light bulb on 
    Given the light bulb is off with <Test>
    When i try to switch on the light with <powerlevel>
     Then the light should be switched on with <powervalue>
  Examples:
  |Test            |powerlevel|powervalue|
  |lowpowertest    |Low       | 20       |
  |mediumpowertest |Medium    |40        |
  |highpowertest   |High      | 60       |
     


  @SwitchOff
  Scenario Outline: Switching the light bulb off
    Given the light bulb is on
    When i try to switch off the light
   Then the light should be switched off

    
